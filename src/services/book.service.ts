import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';

import { CreateBookDto } from 'src/dto/createBook.dto';

import { Book } from 'src/entities/book.entity';

import { ApiError } from 'src/exceptions/ApiError.exception';
import { ErrorMessage } from 'src/exceptions/errorMessage';

@Injectable()
export class BooksService {
  constructor(
    @InjectRepository(Book)
    private booksRepository: Repository<Book>,
  ) {}

  async create(createBookDto: CreateBookDto, userId: string) {
    return await this.booksRepository.save(
      this.booksRepository.create({ ...createBookDto, user: { id: userId } }),
    );
  }

  async findAll(
    page: number,
    size: number,
    author: string,
    publicationYear: number,
  ): Promise<Book[]> {
    if (!isNaN(page) && !isNaN(size)) {
      if (author && !isNaN(publicationYear)) {
        return await this.booksRepository.find({
          where: { author: author, publicationYear: publicationYear },
          take: size,
          skip: size * page,
        });
      } else if (!author) {
        return await this.booksRepository.find({
          where: { publicationYear: publicationYear },
          take: size,
          skip: size * page,
        });
      } else if (isNaN(publicationYear)) {
        return await this.booksRepository.find({
          where: { author: author },
          take: size,
          skip: size * page,
        });
      }
    } else {
      if (author && !isNaN(publicationYear)) {
        return await this.booksRepository.find({
          where: { author: author, publicationYear: publicationYear },
        });
      } else if (!author) {
        return await this.booksRepository.find({
          where: { publicationYear: publicationYear },
        });
      } else if (isNaN(publicationYear)) {
        return await this.booksRepository.find({
          where: { author: author },
        });
      }
    }
    return await this.booksRepository.find();
  }

  async findById(bookId: string): Promise<Book> {
    const book: Book = await this.booksRepository.findOne({
      where: { id: bookId },
      relations: ['user'],
    });

    if (!book) {
      throw ApiError.BadRequest(ErrorMessage.INVALID_REQUEST, ['bookId']);
    }

    return book;
  }

  async updateById(
    bookId: string,
    updateBookDto: CreateBookDto,
    userId: string,
  ): Promise<UpdateResult> {
    const book: Book = await this.findById(bookId);

    if (book.user?.id == userId) {
      return await this.booksRepository.update(
        { id: bookId },
        { ...updateBookDto },
      );
    } else {
      ApiError.ForbiddenError();
    }
  }

  async deleteById(bookId: string, userId: string): Promise<Boolean> {
    const book: Book = await this.findById(bookId);

    if (book.user?.id == userId) {
      const deletes: DeleteResult = await this.booksRepository.delete({
        id: bookId,
      });

      return deletes.affected > 0;
    } else {
      ApiError.ForbiddenError();
    }
  }
}

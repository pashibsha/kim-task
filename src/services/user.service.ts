import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as moment from 'moment';
import * as bcrypt from 'bcrypt';
import { sign, verify } from 'jsonwebtoken';
import { HmacSHA256 } from 'crypto-js';
import { Repository } from 'typeorm';

import { LoginDto } from 'src/dto/login.dto';
import { RegisterDto } from 'src/dto/register.dto';

import { Token } from 'src/entities/token.entity';
import { User } from 'src/entities/user.entity';

import { validateUuid } from 'src/functions/uuid';

import { Payload } from 'src/types/payload.interface';
import { Tokens } from 'src/types/tokens.interface';

import { ApiError } from 'src/exceptions/ApiError.exception';
import { ErrorMessage } from 'src/exceptions/errorMessage';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    @InjectRepository(Token)
    private readonly tokenRepository: Repository<Token>,
  ) {}

  async register(registerDto: RegisterDto): Promise<User> {
    const { password } = registerDto;

    const passwordHashed = await this.passwordHashed(password);

    const createdUser = this.userRepository.create({
      ...registerDto,
      password: passwordHashed,
    });

    await this.userRepository.save(createdUser);

    return await this.findById(createdUser.id);
  }

  async login(loginDto: LoginDto) {
    const user: User = await this.findByLogin(loginDto.email);

    if (!user) {
      throw ApiError.BadRequest(ErrorMessage.INVALID_REQUEST, ['email']);
    }

    const checkPassword: boolean = await this.checkPassword(loginDto);

    if (!checkPassword) {
      throw ApiError.BadRequest(ErrorMessage.INVALID_REQUEST, ['password']);
    }

    const token: Tokens = await this.signPayload(user);

    await this.saveToken(user.id, token.accessToken, token.refreshToken);

    return {
      user,
      accessToken: token.accessToken,
      refreshToken: token.refreshToken,
    };
  }

  async updateRefreshToken(refreshToken: string): Promise<User | null> {
    const user = await this.tokenRepository.findOne({
      where: { refreshToken },
      relations: ['user'],
      select: {
        user: { id: true, name: true, email: true },
      },
    });

    if (!user?.expiresIn) {
      return null;
    }

    const isExpires = moment(new Date().toISOString()).isBefore(user.expiresIn);

    return isExpires ? user.user : null;
  }

  async findById(userId: string): Promise<User> {
    if (!validateUuid(userId)) {
      return null;
    }

    const user = await this.userRepository.findOne({
      where: { id: userId },
      select: { id: true, name: true, email: true },
    });

    return user ? user : null;
  }

  async findByLogin(email: string): Promise<User | null> {
    const user = await this.userRepository.findOne({
      where: { email: email },
    });

    return user ? user : null;
  }

  async findByPayload(payload: Payload): Promise<User> {
    const user = await this.userRepository.findOne({
      where: { id: payload.id, email: payload.email },
      select: { id: true, name: true, email: true },
    });

    if (!user) {
      throw ApiError.BadRequest(ErrorMessage.INVALID_REQUEST, ['user']);
    }

    return user;
  }

  async saveToken(id: string, accessToken: string, refreshToken: string) {
    const token = await this.tokenRepository.findOneBy({ user: { id } });

    const expiresRefresh = new Date(
      moment()
        .add(process.env.REFRESH_EXPIRES_IN || 86400000, 'ms')
        .toISOString(),
    );

    if (!token) {
      const token = this.tokenRepository.create({
        user: { id },
        accessToken,
        refreshToken,
        expiresIn: expiresRefresh,
      });
      await this.tokenRepository.save(token);
    } else {
      token.refreshToken = refreshToken;
      token.accessToken = accessToken;
      token.expiresIn = expiresRefresh;
      await this.tokenRepository.save(token);
    }

    const output = await this.findById(id);

    return output;
  }

  async deleteToken(id: string) {
    const token = await this.tokenRepository.findOne({
      where: { user: { id } },
      relations: ['user'],
    });
    token.refreshToken = null;
    token.accessToken = null;

    await this.tokenRepository.save(token);

    const output = await this.findById(token.user.id);

    return output;
  }

  async checkPassword(loginDto: LoginDto) {
    const user = await this.userRepository.findOne({
      where: {
        email: loginDto.email,
      },
    });

    return await this.passwordVerify(loginDto.password, user.password);
  }

  async passwordHashed(password: string): Promise<string> {
    return await bcrypt.hash(password, 10);
  }

  async passwordVerify(password: string, hash: string): Promise<boolean> {
    return await bcrypt.compare(password, hash);
  }
  async signPayload(user: User): Promise<Tokens> {
    const payload = {
      id: user.id,
      email: user.email,
    };

    const accessToken = sign(payload, process.env.ACCESS_SECRET_KEY, {
      expiresIn: process.env.ACCESS_EXPIRES_IN || 900000,
    });

    const refreshToken = HmacSHA256(
      new Date().getTime().toString(),
      process.env.REFRESH_SECRET_KEY,
    ).toString();

    return { accessToken, refreshToken };
  }

  verifyAccessToken(accessToken: string): any {
    try {
      return verify(accessToken, process.env.ACCESS_SECRET_KEY);
    } catch (error) {
      throw ApiError.UnauthorizedError();
    }
  }

  async validateUser(payload: Payload): Promise<User> {
    return await this.findByPayload(payload);
  }
}

import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { configService } from './config/config.service';

import { UsersModule } from './modules/user.module';
import { BooksModule } from './modules/book.module';

import { User } from './entities/user.entity';
import { Book } from './entities/book.entity';
import { Token } from './entities/token.entity';

import { UsersController } from './controllers/user.controller';
import { BooksController } from './controllers/book.controller';

import { UsersService } from './services/user.service';
import { BooksService } from './services/book.service';

import { JwtStrategy } from './functions/jwt.strategy';

@Module({
  imports: [
    TypeOrmModule.forRoot(configService.getTypeOrmConfig()),
    TypeOrmModule.forFeature([User, Book, Token]),
    UsersModule,
    BooksModule,
  ],
  controllers: [UsersController, BooksController],
  providers: [UsersService, BooksService, JwtStrategy],
})
export class AppModule {}

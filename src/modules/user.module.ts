import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { UsersController } from 'src/controllers/user.controller';

import { UsersService } from 'src/services/user.service';

import { Token } from 'src/entities/token.entity';
import { User } from 'src/entities/user.entity';

@Module({
  imports: [TypeOrmModule.forFeature([User, Token])],
  controllers: [UsersController],
  providers: [UsersService],
})
export class UsersModule {}

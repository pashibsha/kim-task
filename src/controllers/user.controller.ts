import { Body, Controller, Post } from '@nestjs/common';

import { UsersService } from 'src/services/user.service';

import { LoginDto } from 'src/dto/login.dto';
import { RegisterDto } from 'src/dto/register.dto';

import { ApiError } from 'src/exceptions/ApiError.exception';
import { ErrorMessage } from 'src/exceptions/errorMessage';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Post('register')
  async register(@Body() registerDto: RegisterDto) {
    const findUser = await this.usersService.findByLogin(registerDto.email);

    if (findUser) {
      throw ApiError.BadRequest(ErrorMessage.INVALID_REQUEST, ['email']);
    }

    if (!registerDto.name) {
      throw ApiError.BadRequest(ErrorMessage.VALIDATION_ERROR, ['name']);
    }

    const user = await this.usersService.register(registerDto);
    const token = await this.usersService.signPayload(user);

    await this.usersService.saveToken(
      user.id,
      token.accessToken,
      token.refreshToken,
    );

    return {
      user,
      accessToken: token.accessToken,
      refreshToken: token.refreshToken,
    };
  }

  @Post('login')
  async login(@Body() loginDto: LoginDto) {
    const user = await this.usersService.findByLogin(loginDto.email);

    if (!user) {
      throw ApiError.BadRequest(ErrorMessage.INVALID_REQUEST, ['email']);
    }

    const checkPassword = await this.usersService.checkPassword(loginDto);

    if (!checkPassword) {
      throw ApiError.BadRequest(ErrorMessage.INVALID_REQUEST, ['password']);
    }

    const token = await this.usersService.signPayload(user);

    await this.usersService.saveToken(
      user.id,
      token.accessToken,
      token.refreshToken,
    );

    return {
      user,
      accessToken: token.accessToken,
      refreshToken: token.refreshToken,
    };
  }

  @Post('logout')
  async logout(@Body() { refreshToken }: { refreshToken: string }) {
    const user = await this.usersService.updateRefreshToken(refreshToken);

    if (!user) {
      throw ApiError.BadRequest(ErrorMessage.VALIDATION_ERROR, [
        'refreshToken',
      ]);
    }

    await this.usersService.deleteToken(user.id);

    return true;
  }
}

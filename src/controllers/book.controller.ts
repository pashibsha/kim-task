import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

import { User } from 'src/decorators/user.decorator';

import { CreateBookDto } from 'src/dto/createBook.dto';

import { User as UserInterface } from 'src/types/user.interface';

import { BooksService } from 'src/services/book.service';

@Controller('books')
export class BooksController {
  constructor(private readonly booksService: BooksService) {}

  @UseGuards(AuthGuard('jwt'))
  @Post()
  async create(
    @Body() createBookDto: CreateBookDto,
    @User() user: UserInterface,
  ) {
    return this.booksService.create(createBookDto, user.id);
  }

  @Get()
  async findAll(
    @Query('page') page: number,
    @Query('size') size: number,
    @Query('author') author: string,
    @Query('publicationYear') publicationYear: number,
  ) {
    return this.booksService.findAll(page, size, author, publicationYear);
  }

  @Get(':bookId')
  async find(@Param('bookId') bookId: string) {
    return this.booksService.findById(bookId);
  }

  @UseGuards(AuthGuard('jwt'))
  @Put(':bookId')
  async update(
    @Param('bookId') bookId: string,
    @Body() updateBookDto: CreateBookDto,
    @User() user: UserInterface,
  ) {
    return this.booksService.updateById(bookId, updateBookDto, user.id);
  }

  @UseGuards(AuthGuard('jwt'))
  @Delete(':bookId')
  async delete(@Param('bookId') bookId: string, @User() user: UserInterface) {
    return this.booksService.deleteById(bookId, user.id);
  }
}
